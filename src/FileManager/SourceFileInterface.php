<?php

namespace Kisphp\FileManager;

interface SourceFileInterface
{
    /**
     * @return string
     */
    public function getFileName();

    /**
     * @return string
     */
    public function getFilePath();
}
