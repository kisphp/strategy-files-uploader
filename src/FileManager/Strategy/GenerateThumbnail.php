<?php

namespace Kisphp\FileManager\Strategy;

use Kisphp\FileManager\AbstractStrategy;
use Kisphp\FileManager\SourceFileInterface;
use Kisphp\ImageResizer;

class GenerateThumbnail extends AbstractStrategy
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @param string $uploadPath
     * @param int $width
     * @param int $height
     */
    public function __construct($uploadPath, $width = 0, $height = 0)
    {
        parent::__construct($uploadPath);

        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return string
     */
    public function getThumbnailPath(SourceFileInterface $sourceFile)
    {
        return $this->uploadPath . $this->width . 'x' . $this->height . '_' . $this->getFileName($sourceFile);
    }

    /**
     * @param SourceFileInterface $sourceFile
     * @param int $width
     * @param int $height
     *
     * @return SourceFileInterface
     *
     * @throws \Kisphp\ImageFileTypeNotAllowed
     */
    public function execute(SourceFileInterface $sourceFile)
    {
        $destination = $this->getThumbnailPath($sourceFile);

        $imageResizer = new ImageResizer();
        $imageResizer->load($sourceFile->getFilePath());
        $imageResizer->setTarget($destination);
        $imageResizer->resize($this->width, $this->height);
        $imageResizer->save();

        return $sourceFile;
    }

    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return null|string|string[]
     */
    private function getFileName(SourceFileInterface $sourceFile)
    {
        $filename = $sourceFile->getFileName();
        $filename = preg_replace('/([\d]+)x([\d]+)\_/', '', $filename);

        return $filename;
    }
}
