<?php

namespace Kisphp\FileManager\Strategy;

use Kisphp\FileManager\AbstractStrategy;
use Kisphp\FileManager\SourceFile;
use Kisphp\FileManager\SourceFileInterface;

class UniqIdName extends AbstractStrategy
{
    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return SourceFileInterface
     */
    public function execute(SourceFileInterface $sourceFile)
    {
        $extension = $this->getExtension($sourceFile);

        $destination = $this->uploadPath . uniqid() . '.' . $extension;

        copy(
            $sourceFile->getFilePath(),
            $destination,
        );

        return new SourceFile($destination);
    }

    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return string
     */
    protected function getExtension(SourceFileInterface $sourceFile)
    {
        $filename = $sourceFile->getFileName();

        $chunks = explode('.', $filename);

        return (string) end($chunks);
    }
}
