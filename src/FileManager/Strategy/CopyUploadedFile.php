<?php

namespace Kisphp\FileManager\Strategy;

use Kisphp\FileManager\AbstractStrategy;
use Kisphp\FileManager\SourceFile;
use Kisphp\FileManager\SourceFileInterface;

class CopyUploadedFile extends AbstractStrategy
{
    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return SourceFile|SourceFileInterface
     */
    public function execute(SourceFileInterface $sourceFile)
    {
        $destinationFilePath = $this->uploadPath . strtolower($sourceFile->getFileName());

        move_uploaded_file(
            $sourceFile->getFilePath(),
            $destinationFilePath
        );

        return new SourceFile($destinationFilePath);
    }
}
