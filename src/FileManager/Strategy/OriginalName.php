<?php

namespace Kisphp\FileManager\Strategy;

use Kisphp\FileManager\AbstractStrategy;
use Kisphp\FileManager\SourceFile;
use Kisphp\FileManager\SourceFileInterface;

class OriginalName extends AbstractStrategy
{
    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return SourceFile|SourceFileInterface
     */
    public function execute(SourceFileInterface $sourceFile)
    {
        $destination = $this->uploadPath . $sourceFile->getFileName();

        copy(
            $sourceFile->getFilePath(),
            $destination,
        );

        return new SourceFile($destination);
    }
}
