<?php

namespace Kisphp\FileManager;

abstract class AbstractStrategy implements FileStrategyInterface
{
    /**
     * @var string
     */
    protected $uploadPath;

    /**
     * @param string $uploadPath
     */
    public function __construct($uploadPath)
    {
        $this->uploadPath = $uploadPath;

        if (!is_dir($this->uploadPath)) {
            mkdir($this->uploadPath, 0755, true);
        }
    }
}
