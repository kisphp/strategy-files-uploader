<?php

namespace Kisphp\FileManager;

interface FileStrategyInterface
{
    /**
     * @param SourceFileInterface $sourceFile
     *
     * @return SourceFileInterface
     */
    public function execute(SourceFileInterface $sourceFile);
}
