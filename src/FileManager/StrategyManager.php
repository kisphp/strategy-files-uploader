<?php

namespace Kisphp\FileManager;

class StrategyManager
{
    /**
     * @var FileStrategyInterface[]
     */
    protected $strategies;

    /**
     * @param FileStrategyInterface $strategy
     *
     * @return $this
     */
    public function chain(FileStrategyInterface $strategy)
    {
        $this->strategies[] = $strategy;

        return $this;
    }

    /**
     * @param SourceFileInterface $sourceFile
     */
    public function executeChain(SourceFileInterface $sourceFile)
    {
        foreach ($this->strategies as $strategy) {
            $sourceFile = $strategy->execute($sourceFile);
        }
    }
}
