<?php

namespace Kisphp\FileManager;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedSourceFile implements SourceFileInterface
{
    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var null|string
     */
    protected $fileName;

    /**
     * @param UploadedFile $filePath
     */
    public function __construct(UploadedFile $filePath)
    {
        $this->filePath = $filePath->getPathname();
        $this->fileName = $filePath->getClientOriginalName();
    }

    /**
     * @return null|string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }
}
