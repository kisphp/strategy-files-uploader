# Kisphp File Uploader strategy

[![pipeline status](https://gitlab.com/marius-rizac/strategy-files-uploader/badges/master/pipeline.svg)](https://gitlab.com/marius-rizac/strategy-files-uploader/-/commits/master)
[![coverage report](https://gitlab.com/marius-rizac/strategy-files-uploader/badges/master/coverage.svg)](https://gitlab.com/marius-rizac/strategy-files-uploader/-/commits/master)

Upload files and generate thumbnails using Strategy pattern

### Installation

```bash
composer require kisphp/strategy-files-uploader
```

### Usage

```php
<?php

use Kisphp\FileManager\Strategy\CopyUploadedFile;
use Kisphp\FileManager\Strategy\GenerateThumbnail;
use Kisphp\FileManager\StrategyManager;
use Kisphp\FileManager\UploadedSourceFile;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/path/to/vendor/autoload.php';

$req = Request::createFromGlobals();

$sourceFilePath = __DIR__ . '/path/to/uploads/';
$targetFilePath = __DIR__ . '/path/to/public/thumbs/';

$sourceImage = new UploadedSourceFile($req->files->get('file'));

$sm = new StrategyManager();

$sm
    ->chain(new CopyUploadedFile($sourceFilePath))
    ->chain(new GenerateThumbnail($targetFilePath, 300, 250))
    ->chain(new GenerateThumbnail($targetFilePath, 64, 64))
    ->chain(new GenerateThumbnail($targetFilePath, 800, 0))
;

$sm->executeChain($sourceImage);
```

- `GenerateThumbnail` will return the source file object. This way, you will not make a new thumbnail from the previous thumbnail
- `CopyUploadedFile`, `UniqIdName` and `OriginalName` will return a `SourceFile` object form the copied file
