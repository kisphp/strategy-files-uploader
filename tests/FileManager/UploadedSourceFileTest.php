<?php

namespace tests\FileManager;

use Kisphp\FileManager\SourceFile;
use Kisphp\FileManager\UploadedSourceFile;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedSourceFileTest extends TestCase
{
    const TMP_FILE_JPG = '/tmp/file-2.jpg';

    protected function tearDown(): void
    {
        parent::tearDown();

        unlink(self::TMP_FILE_JPG);
    }

    public function test_instance()
    {
        touch(self::TMP_FILE_JPG);

        $file = \Mockery::mock(UploadedFile::class);
        $file->shouldReceive('getPathName')->andReturn(self::TMP_FILE_JPG);
        $file->shouldReceive('getClientOriginalName')->andReturn('file-2.jpg');

        $s = new UploadedSourceFile($file);

        $this->assertInstanceOf(UploadedSourceFile::class, $s);

        $this->assertSame('/tmp/file-2.jpg', $s->getFilePath());
        $this->assertSame('file-2.jpg', $s->getFileName());
    }
}
