<?php

namespace FileManager;

use Kisphp\FileManager\FileStrategyInterface;
use Kisphp\FileManager\SourceFileInterface;
use Kisphp\FileManager\StrategyManager;
use PHPUnit\Framework\TestCase;

class StrategyManagerTest extends TestCase
{
    public function test_strategy_manager()
    {
        $sm = new StrategyManager();
        $sfi = \Mockery::mock(SourceFileInterface::class);

        $str1 = \Mockery::mock(FileStrategyInterface::class);
        $str1->shouldReceive('execute')->withArgs([SourceFileInterface::class])->andReturn($sfi);

        $str2 = \Mockery::mock(FileStrategyInterface::class);
        $str2->shouldReceive('execute')->withArgs([SourceFileInterface::class])->andReturn($sfi);



        $sm
            ->chain($str1)
            ->chain($str2)
        ;

        $this->assertNull($sm->executeChain($sfi));
    }
}
