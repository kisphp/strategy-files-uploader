<?php

namespace FileManager\Strategy;

use Kisphp\FileManager\SourceFileInterface;
use Kisphp\FileManager\Strategy\CopyUploadedFile;
use Kisphp\FileManager\Strategy\GenerateThumbnail;
use Kisphp\FileManager\Strategy\OriginalName;
use PHPUnit\Framework\TestCase;

class GenerateThumbnailTest extends TestCase
{
    const FILE_NAME = 'file-3.jpg';

    public function test_original_name()
    {
        $tmpFile = $this->generateTestImage();

        $sfi = \Mockery::mock(SourceFileInterface::class);
        $sfi->shouldReceive('getFileName')->andReturn('source-resize.jpg');
        $sfi->shouldReceive('getFilePath')->andReturn($tmpFile);

        $str = new GenerateThumbnail(sys_get_temp_dir() . '/dest/', 300, 250);

        $this->assertInstanceOf(SourceFileInterface::class, $str->execute($sfi));

        $thumb = getimagesize($str->getThumbnailPath($sfi));
        $this->assertSame(300, $thumb[0]);
        $this->assertSame(250, $thumb[1]);
    }

    /**
     * @return string
     */
    protected function getTempPath()
    {
        return sys_get_temp_dir() . '/' . self::FILE_NAME;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        exec('rm -rf ' . sys_get_temp_dir() . '/dest/');
    }

    /**
     * @return string
     */
    protected function generateTestImage()
    {
        $filePath = sys_get_temp_dir() . '/source-resize.jpg';

        $im = imagecreate(800, 600);
        imagecolorallocate($im, 0, 0, 0);
        $textColor = imagecolorallocate($im, 233, 14,91);
        imagestring($im, 1, 5,5,"A simple text string", $textColor);
        imagejpeg($im, $filePath);
        imagedestroy($im);

        return $filePath;
    }
}
