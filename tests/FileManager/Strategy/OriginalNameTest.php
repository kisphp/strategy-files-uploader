<?php

namespace FileManager\Strategy;

use Kisphp\FileManager\SourceFileInterface;
use Kisphp\FileManager\Strategy\OriginalName;
use PHPUnit\Framework\TestCase;

class OriginalNameTest extends TestCase
{
    const FILE_NAME = 'file-3.jpg';

    public function test_original_name()
    {
        $str = new OriginalName(sys_get_temp_dir() . '/dest/');

        touch($this->getTempPath());

        $sfi = \Mockery::mock(SourceFileInterface::class);
        $sfi->shouldReceive('getFileName')->andReturn(self::FILE_NAME);
        $sfi->shouldReceive('getFilePath')->andReturn($this->getTempPath());

        $this->assertInstanceOf(SourceFileInterface::class, $str->execute($sfi));
    }

    /**
     * @return string
     */
    protected function getTempPath()
    {
        return sys_get_temp_dir() . '/' . self::FILE_NAME;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        exec('rm -rf ' . sys_get_temp_dir() . '/dest/');
    }
}
