<?php

namespace FileManager\Strategy;

use Kisphp\FileManager\SourceFileInterface;
use Kisphp\FileManager\Strategy\CopyUploadedFile;
use Kisphp\FileManager\Strategy\OriginalName;
use PHPUnit\Framework\TestCase;

class CopyUploadedFileTest extends TestCase
{
    const FILE_NAME = 'file-3.jpg';

    public function test_original_name()
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'cuf');

        $sfi = \Mockery::mock(SourceFileInterface::class);
        $sfi->shouldReceive('getFileName')->andReturn(self::FILE_NAME);
        $sfi->shouldReceive('getFilePath')->andReturn($tmpFile);

        $str = new CopyUploadedFile(sys_get_temp_dir() . '/dest/');

        $this->assertInstanceOf(SourceFileInterface::class, $str->execute($sfi));
    }

    /**
     * @return string
     */
    protected function getTempPath()
    {
        return sys_get_temp_dir() . '/' . self::FILE_NAME;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        exec('rm -rf ' . sys_get_temp_dir() . '/dest/');
    }
}
