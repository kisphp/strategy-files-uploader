<?php

namespace tests\FileManager;

use Kisphp\FileManager\SourceFile;
use PHPUnit\Framework\TestCase;

class SourceFileTest extends TestCase
{
    const TMP_FILE_1_JPG = '/tmp/file-1.jpg';

    protected function tearDown(): void
    {
        parent::tearDown();

        unlink(self::TMP_FILE_1_JPG);
    }

    public function test_instance()
    {
        touch(self::TMP_FILE_1_JPG);

        $s = new SourceFile(self::TMP_FILE_1_JPG);

        $this->assertInstanceOf(SourceFile::class, $s);

        $this->assertSame('/tmp/file-1.jpg', $s->getFilePath());
        $this->assertSame('file-1.jpg', $s->getFileName());
    }
}
