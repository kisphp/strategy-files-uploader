<?php

use Kisphp\FileManager\Strategy\CopyUploadedFile;
use Kisphp\FileManager\Strategy\GenerateThumbnail;
use Kisphp\FileManager\StrategyManager;
use Kisphp\FileManager\UploadedSourceFile;
use Kisphp\Utils\Files;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

$req = Request::createFromGlobals();

if ($req->isMethod('POST')) {

    $generatedDirectory = Files::createTargetDirectory(__DIR__ . '/../data/source/');

    $sourceFilePath = __DIR__ . '/../data/source/' . $generatedDirectory . '/';
    $targetFilePath = __DIR__ . '/../example/images/' . $generatedDirectory . '/';

    $sourceImage = new UploadedSourceFile($req->files->get('file'));

    $sm = new StrategyManager();

    $sm
        ->chain(new CopyUploadedFile($sourceFilePath))
        ->chain(new GenerateThumbnail($targetFilePath, 300, 250))
        ->chain(new GenerateThumbnail($targetFilePath, 64, 64))
        ->chain(new GenerateThumbnail($targetFilePath, 800, 0))
    ;

    $sm->executeChain($sourceImage);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="/index.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="test" value="ok">
    <input type="file" name="file" />
    <input type="submit" value="Upload" />
</form>

</body>
</html>
